<?php
    include 'functions.php';
    //add an XML header to the output
    header('Content-type: text/xml');
    //prompt the browser to download the output as a file called offer.xml
    header('Content-Disposition: attachment; filename="offer.xml"');
    export_offer();
?>
