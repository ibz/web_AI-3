<?php
include 'functions.php';
?>
<!DOCTYPE html>
<html>
<head>
  <!-- enable utf-8 encoding for umlauts etc.-->
  <meta charset="utf-8">
  <!-- Description of what this dose -->
  <meta name ="viewport" content="width=device-width, initial-scale=1">
  <!-- link to the default css file -->
  <link rel="stylesheet" href="css/stylesheet.css"/>
  <title>Businessstreamline</title>
</head>

<body>
    <div>
      <header>
        <h1>Demand Details</h1>
        <!-- The sidebar naviagtion begins here -->
        <nav>
          <?php
          include 'navigation.php';
          ?>
        </nav>
        <!-- The sidebar naviagtion ends here -->
      </header>
    </div>
    <!-- The Post function begins here  -->
    <div class='post-box'><p>
      <?php
        show_demand();
      ?>
    </div>
    <?php
        show_offer();
    ?>
</body>
</html>
