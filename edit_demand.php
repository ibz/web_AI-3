<?php
include 'functions.php';
date_default_timezone_set('Europe/Amsterdam');
?>
<!DOCTYPE html>
<html>

<head>
  <!-- enable utf-8 encoding for umlauts etc.-->
  <meta charset="utf-8">
  <!-- Description of what this dose -->
  <meta name ="viewport" content="width=device-width, initial-scale=1">
  <!-- link to the default css file -->
  <link rel="stylesheet" href="css/stylesheet.css"/>
  <title>Businessstreamline</title>
</head>
<body>
  <div>
    <div>
      <header>
        <!-- The title begins here -->
        <h1>Edit</h1>
        <!-- The title ends here -->

        <!-- The sidebar naviagtion begins here -->
        <nav>
          <?php
          include 'navigation.php';
          ?>
        </nav>
        <!-- The sidebar naviagtion ends here -->
      </header>
    </div>
    <!-- Edit function begins here -->
      <h2>Edit Board</h2>

      <?php
      $userId = $_POST['userId'];
      $demandId = $_POST['demandId'];
      $piecesMax = $_POST['piecesMax'];
      $piecesMin = $_POST['piecesMin'];
      $deliveryDate = $_POST['deliveryDate'];
      $title= $_POST['title'];
      $text = $_POST['text'];

      echo "<form class= 'post-box p' method='POST' action='".edit_demand()."'>
          Title<br>
          <input type='text' name='title' value='".$title."'><br>
          <input type='hidden' name='userId' value='".$userId."'>
          <input type='hidden' name='demandId' value='".$demandId."'><br />
          Maximum required pieces<br/>
          <input type='number' name='piecesMax' value='".$piecesMax."'><br><br>
          Minimum required pieces<br/>
          <input type='number' name='piecesMin' value='".$piecesMin."'><br><br>
          Delivery Date in YYYY-MM-DD format<br>
          <input type='date' name='deliveryDate' value='".$deliveryDate."'><br>
          <br>
          Description<br>
          <textarea name='text'>".$text."</textarea><br />
          <button type='submit' name='edit'>Edit</button>
          </form>";
      ?>
  </div>
</body>
</html>
