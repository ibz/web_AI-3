<?php
include 'functions.php';
date_default_timezone_set('Europe/Amsterdam');
?>
<!DOCTYPE html>
<html>
<head>
  <!-- enable utf-8 encoding for umlauts etc.-->
  <meta charset="utf-8">
  <!-- Description of what this dose -->
  <meta name ="viewport" content="width=device-width, initial-scale=1">
  <!-- link to the default css file -->
  <link rel="stylesheet" href="css/stylesheet.css"/>
</head>

<body>
  <div>
    <div>
      <header>
        <!-- The title begins here -->
        <h1>Search Page</h1>
        <!--The Title ends here -->

        <!-- The sidebar naviagtion begins here -->
        <nav>
          <?php
          include 'navigation.php';
          ?>
        </nav>
        <!-- The sidebar naviagtion ends here -->
      </header>
      <div class= "">
        <?php
        if (isset($_POST['submit-search'])) {
            global $con;

            $search = mysqli_real_escape_string($con, $_POST['search']);
            $sql = "SELECT * FROM demands WHERE demandTitle LIKE '%$search%'
            OR demandText LIKE '%$search%'";
            $result= mysqli_query($con, $sql);
            $queryResult = mysqli_num_rows($result);

            if($queryResult > 0) {
              while($row = mysqli_fetch_assoc($result)){
                echo "<div class='post-box'><p>";
                  echo "<strong>" . $row['demandTitle'] . "</strong>" . "<br>";
                  echo $row['date'] . "<br>";
                  echo "</p>";
                  echo "<form method= 'POST' action='show_demand.php'>
                  <input type='hidden' name='demandId' value='".$row['demandId']."'>
                  <button>Show Details</button>
                  </form>
                  </div>";
              }
            }else{
              echo "There are no results matching your search!";
            }
          }
         ?>

      </div>
    </div>
  </body>
  </html>
