<?php
include 'functions.php';
?>
<!DOCTYPE html>
<html>
<head>
  <!-- enable utf-8 encoding for umlauts etc.-->
  <meta charset="utf-8">
  <!-- Description of what this dose -->
  <meta name ="viewport" content="width=device-width, initial-scale=1">
  <!-- link to the default css file -->
  <link rel="stylesheet" href="css/loginstyle.css"/>
  <title>Businessstreamline</title>
</head>

<body>
<div>
<div>
    <header>
    <!-- The title begins here -->
    <h1>B2B2-Plattform</h1>
    </header>
</div>
<div class="form">
    <h1>Registration</h1>
    <form name="registration" action="<?= $_SERVER['PHP_SELF'] ?>" method="post">
        <input type="text" name="username" placeholder="Username" required />
        <input type="email" name="email" placeholder="Email" required />
        <input type="password" name="password" placeholder="Password" required />
        <input type="submit" name="submit" value="Register"/>
    </form>
    <?php
    register();
    ?>
</div>
</div>
</body>
</html>
