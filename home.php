<!DOCTYPE html>
<html>

<head>
  <!-- enable utf-8 encoding for umlauts etc.-->
  <meta charset="utf-8">
  <!-- Description of what this dose -->
  <meta name ="viewport" content="width=device-width, initial-scale=1">
  <!-- link to the default css file -->
  <link rel="stylesheet" href="css/stylesheet.css"/>
  <title>Businessstreamline</title>
</head>

<body>
  <div>
    <div>
      <header>
        <!-- The title begins here -->
        <h1>Home</h1>
        <!-- The title begins here -->

        <!-- The sidebar naviagtion begins here -->
        <nav>
          <?php
          include 'navigation.php';
          ?>
        </nav>
        <!-- The sidebar naviagtion ends here -->
      </header>
    </div>
  </div>
</body>
</html>
