<?php
include 'functions.php';
date_default_timezone_set('Europe/Amsterdam');
?>
<!DOCTYPE html>
<html>
<head>
  <!-- enable utf-8 encoding for umlauts etc.-->
  <meta charset="utf-8">
  <!-- Description of what this dose -->
  <meta name ="viewport" content="width=device-width, initial-scale=1">
  <!-- link to the default css file -->
  <link rel="stylesheet" href="css/stylesheet.css"/>
  <title>Businessstreamline</title>
</head>

<body>
    <div>
      <header>
        <!-- The title begins here -->
        <h1>Board</h1>
        <!--The Title ends here -->

        <!-- The sidebar naviagtion begins here -->
        <nav>
          <?php
          include 'navigation.php';
          ?>
        </nav>
        <!-- The sidebar naviagtion ends here -->
      </header>
    </div>
    <!-- The Post function begins here  -->
    <div>
      <div>
      <h2>Open Demands</h2>

      <form action='search_demand.php' method='POST' >
           <input type ="text" name = "search" placeholder="search">
           <button type ="submit" name="submit-search">Search</button>
      </form>

    </div>
      <?php
      get_demand_titles($con);
      ?>
    </div>
</body>
</html>
