<?php
include 'functions.php';
date_default_timezone_set('Europe/Amsterdam');
?>
<!DOCTYPE html>
<html>
<head>
  <!-- enable utf-8 encoding for umlauts etc.-->
  <meta charset="utf-8">
  <!-- Description of what this dose -->
  <meta name ="viewport" content="width=device-width, initial-scale=1">
  <!-- link to the default css file -->
  <link rel="stylesheet" href="css/stylesheet.css"/>
  <title>Businessstreamline</title>
</head>

<body>
    <div>
      <header>
        <!-- The title begins here -->
        <h1>Board</h1>
        <!--The Title ends here -->

        <!-- The sidebar naviagtion begins here -->
        <nav>
          <?php
          include 'navigation.php';
          ?>
        </nav>
        <!-- The sidebar naviagtion ends here -->
      </header>
    </div>

    <!-- The Post function begins here  -->
    <div>
      <h2>New entry</h2>
      <form class= 'post-box p' method='POST' action='<?= $_SERVER['PHP_SELF'] ?>'>
          Title<br>
          <input type='text' name='title' required><br>
          <br>
          Quality<br>
          <select name='quality'>
          <option selected='selected' value='1'>New</option>
          <option value='2'>Used</option>
          </select><br>
          <br>
          Maximum required pieces<br>
          <input type='number' name='piecesMax' value='' required><br>
          <br>
          Minimum required pieces<br>
          <input type='number' name='piecesMin' value='' required><br>
          <br>
          Delivery Date in YYYY-MM-DD format<br>
          <input type='date' name='deliveryDate' value='' required><br>
          <br>
          Description<br>
          <textarea rows='6' cols='40' name='text' required></textarea><br>
          <button type='submit' name='submit'>SUBMIT</button>
          </form>
    </div>
    <?php
      post_demand();
    ?>
</body>
</html>
