<?php
//load the database functions
require('db.php');

/* the function which verifies a user's credentials
   against the database */
function login ()
{
    session_start();
    if (isset($_REQUEST['username'])) {
        global $con;

        /* assign the variables with data from the login form */
        $username = stripslashes($_REQUEST['username']);
        //escapes special characters in a string
        $username = mysqli_real_escape_string($con,$username);
        global $current_user;
        $current_user = $username;
        $password = stripslashes($_REQUEST['password']);
        $password = mysqli_real_escape_string($con,$password);

        //check the form data against the database
        $query = "SELECT * FROM users WHERE userLogin='$username'
                  and userPass='$password'";
        $result = mysqli_query($con,$query) or die(mysqli_error());

        /* forward the user to the home.php page if the login is correct
           otherwise prompt an error message*/
        $rows = mysqli_num_rows($result);
        if ($rows==1) {
            $_SESSION['username'] = $username;
            // Redirect user to home.php
            header("Location: home.php");
        } else {
            echo "<h3>Username/password is incorrect.</h3>";
        }
    }
}

// A function to register a new user
function register ()
{
    if (isset($_REQUEST['username'])) {
        global $con;

        /* assign the variables with data from the login form */
        // removes backslashes
        $username = stripslashes($_REQUEST['username']);
        //escapes special characters in a string
        $username = mysqli_real_escape_string($con,$username);
        $email = stripslashes($_REQUEST['email']);
        $email = mysqli_real_escape_string($con,$email);
        $password = stripslashes($_REQUEST['password']);
        $password = mysqli_real_escape_string($con,$password);

        /* create a new user entry inside the database */
        $query = "INSERT into users (userLogin, userPass, userEmail)
                  VALUES ('$username', '$password', '$email')";
        $result = mysqli_query($con,$query);

        /* tell the user if the username or the email address are
           already present in the database based on the return code
           of the database*/
        if ($result) {
            echo "<div class='form'>
                  <h3>You are registered successfully.</h3>
                  <br/>Click here to <a href='index.php'>Login</a></div>";
        } elseif (mysqli_errno($con) == 1062) {
            echo "<h3>Username or Email already taken.</h3>";
        }
    }
}

/*A function to get the userId from the database for the current
  logged in user based on the username used to login*/
function get_userid ()
{
    global $con;
    session_start();
    $username = $_SESSION['username'];
    $sql = "select userId from users where userLogin = '$username'";
    $userId = mysqli_query($con, $sql) or die(mysqli_error($con));
    $row = $userId->fetch_object();
    return $row->userId;
}

// A function to post a demand
function post_demand ()
{
    if (isset($_POST['submit'])) {
        global $con;
        $userId = get_userid();

        /* assign the variables with data from the demand form */
        $piecesMax = $_POST['piecesMax'];
        $piecesMin = $_POST['piecesMin'];
        $qualityId = $_POST['quality'];
        $deliveryDate = $_POST['deliveryDate'];
        $title = $_POST['title'];
        $text = $_POST['text'];

        /* check if the values make sense before adding them to
           the databse */
        if ($piecesMax <= $piecesMin) {
            echo "<strong class='warning'>
                  The Maximum must be bigger than the Minimum!
                  </strong>";
        } else {
            $sql = "INSERT INTO demands (piecesMax, piecesMin, demandText,
                                        demandTitle, userId, deliveryDate,
                                        qualityId)
                    VALUES ('$piecesMax', '$piecesMin', '$text', '$title',
                            '$userId', '$deliveryDate', '$qualityId')";
            $result = mysqli_query($con, $sql) or die(mysqli_error($con));
        }
    }
}

// A function to insert the demand titles from the database into the website
function get_demand_titles ()
{
    global $con;

    //get the data from the database
    $sql = "SELECT * FROM demands";
    $result = mysqli_query($con, $sql);

    /*fetch the data from the $result array and insert it into
      the corresponding HTML code */
    while ($row = $result->fetch_assoc()) {
        echo "<div class='post-box'><p>";
        echo "<strong>" . $row['demandTitle'] . "</strong>" . "<br>";
        echo $row['date'] . "<br>";

        //add a delete button to the demand output
        echo "<form class= 'delete-form' method= 'POST'
        action='".delete_demand()."'>
        <input type='hidden' name='demandId' value='".$row['demandId']."'>
        <button type='submit' name= 'deletepost'> Delete</button>
        </form>";

        /*add a button to the demand output which let's the user view
        the demand details */
        echo "<form method= 'POST' action='show_demand.php'>
        <input type='hidden' name='demandId' value='".$row['demandId']."'>
        <button>Show Details</button>
        </form>";

        /* add an edit button to the demand output and give it the
           necessary data to edit. */
        echo "<form class= 'edit-form' method= 'POST' action='edit_demand.php'>
        <input type='hidden' name='userId' value='".$row['userId']."'>
        <input type='hidden' name='demandId' value='".$row['demandId']."'>
        <input type='hidden' name='piecesMax' value='".$row['piecesMax']."'>
        <input type='hidden' name='piecesMin' value='".$row['piecesMin']."'>
        <input type='hidden' name='deliveryDate' value='".$row['deliveryDate']."'>
        <input type='hidden' name='title' value='".$row['demandTitle']."'>
        <input type='hidden' name='text' value='".$row['demandText']."'>
        <button>Edit</button>
        </form>
        </div>";
    }
}


// A function to edit a demand
function edit_demand ()
{
    if (isset($_POST['edit'])) {
        global $con;

        /* assign the variables with data from the edit button */
        $demandId = $_POST['demandId'];
        $piecesMax = $_POST['piecesMax'];
        $piecesMin = $_POST['piecesMin'];
        $deliveryDate = $_POST['deliveryDate'];
        $title = $_POST['title'];
        $text = $_POST['text'];

        /* Inserts the updates into the database if the values
           make sense otherwise inform the user about his error*/
        if ($piecesMax <= $piecesMin) {
            echo "<strong class='warning'>
                  The Maximum must be bigger than the Minimum!
                  </strong>";
        } else {
            $sql = "UPDATE demands
                        SET piecesMax='$piecesMax',
                            piecesMin='$piecesMin',
                            demandTitle='$title',
                            demandText='$text',
                            deliveryDate='$deliveryDate'
                        WHERE demandId='$demandId'";
            $result = mysqli_query($con, $sql) or die(mysqli_error($con));
            header("Location: board.php");
        }
    }
}

// A function to delete a post
function delete_demand()
{
    if (isset($_POST['deletepost'])) {
        global $con;

        /* assign the variable with data from the delete button */
        $demandId = $_POST['demandId'];

        /* Delete all the offers corresponding to the demandId
           from the database */
        $delete_offers_query = "DELETE FROM offers WHERE demandId='$demandId'";
        $delete_offers_result = mysqli_query($con, $delete_offers_query)
                             or die(mysqli_error($con));

        // Delete the demand matching the demandId from the database
        $delete_demand_query = "DELETE FROM demands WHERE demandId='$demandId'";
        $delete_demand_result = mysqli_query($con, $delete_demand_query)
                             or die(mysqli_error($con));

        header('Location: board.php');
  }
}

//a function to show a demand's details
function show_demand ()
{
    global $con;

    /* assign the variable with data from the details button */
    $demandId = $_POST['demandId'];

    //get the corresponding data from the database
    $demand_query = "SELECT * FROM demands where demandId='$demandId'";
    $demand_query_result = mysqli_query($con, $demand_query) or
                         die(mysqli_error($con));
    $demand_rows = $demand_query_result->fetch_object();

    //get the quality requested in the demand from the database
    $qualityId = $demand_rows->qualityId;
    $quality_query = "SELECT * FROM quality where qualityId='$qualityId'";
    $quality_query_result = mysqli_query($con, $quality_query) or
                          die(mysqli_error($con));
    $quality_rows = $quality_query_result->fetch_object();

    //print the demand details
    echo "<h2>" . $demand_rows->demandTitle . "</h2>";
    echo "<br>
          <br>";
    echo "<strong>Maximum required pieces: </strong>" . $demand_rows->piecesMax;
    echo "<br>
          <br>";
    echo "<strong>Minimum required pieces: </strong>" . $demand_rows->piecesMin;
    echo "<br>
          <br>";
    echo "<strong>Desired Date of Delivery: </strong>".
         $demand_rows->deliveryDate;
    echo "<br>
          <br>";
    echo "<strong>Desired Quality: </strong>" . $quality_rows->qualityName;
    echo "<br>
          <br>
          <strong>Description:</strong><br>";
    echo "$demand_rows->demandText<br><br>";

    /*add a "Post Offer" button to the demand's details to let a user
      post an offer */
    echo "<form method= 'POST' action='add_offer.php'>
         <input type='hidden' name='demandId' value='$demand_rows->demandId'>
         <button>Post Offer</button>
         </form><br>";
}

//A function to post an offer to a demand
function post_offer()
{
    if (isset($_POST['submit'])) {
        global $con;

        //get the current user's ID
        $userId = get_userid();

        /* assign the variables with data from the "Post Offer" form*/
        $demandId = $_POST['demandId'];
        $pieces = $_POST['pieces'];
        $price = $_POST['price'];
        $qualityId = $_POST['quality'];
        $text = $_POST['text'];

        // Insert the offer into the database
        $sql = "INSERT INTO offers (pieces, text, price, userId, qualityId,
                                    demandId)
               VALUES ('$pieces', '$text', '$price', '$userId', '$qualityId',
                       '$demandId')";
        $result = mysqli_query($con, $sql) or die(mysqli_error($con));
        header('Location: board.php');
    }
}

//A function to print out the offers to a demand
function show_offer ()
{
    global $con;

    /* assign the variables with data from demand*/
    $demandId = $_POST['demandId'];

    // get the offers matching the demand from the database
    $offer_query = "SELECT * FROM offers where demandId='$demandId'";
    $offer_query_result = mysqli_query($con, $offer_query) or
                         die(mysqli_error($con));

    // if the query on the database returned data print the data
    while ($offer_rows = $offer_query_result->fetch_object()) {

        //get the corresponding quality data from the database
        $qualityId = $offer_rows->qualityId;
        $quality_query = "SELECT * FROM quality where qualityId='$qualityId'";
        $quality_query_result = mysqli_query($con, $quality_query) or
                            die(mysqli_error($con));
        $quality_rows = $quality_query_result->fetch_object();

        echo "<strong>Offer Nr: </strong>" . $offer_rows->offerId;
        echo "<br>
            <br>";
        echo "<strong>Amount available: </strong>" . $offer_rows->pieces;
        echo "<br>
            <br>";
        echo "<strong>Price per piece: </strong>" . $offer_rows->price;
        echo "<br>
            <br>";
        echo "<strong>Provided Quality: </strong>" . $quality_rows->qualityName;
        echo "<br>
            <br>
            <strong>Description:</strong><br>";
        echo $offer_rows->text . "<br><br>";

        /*Add a button to each offer which let's the user export the offer*/
        echo "<form method= 'POST' action='export_offer.php'>
         <input type='hidden' name='offerId' value='$offer_rows->offerId'>
         <button>Export this Offer</button>
         </form>";

        //add a delete button to the offer output
        echo "<form method= 'POST' action='".delete_offer()."'>
        <input type='hidden' name='offerId' value='$offer_rows->offerId'>
        <input type='hidden' name='demandId' value='$demandId'>
        <button type='submit' name= 'delete_offer'> Delete</button>
        </form><hr>";
    }
}

function delete_offer()
{
    if (isset($_POST['delete_offer'])) {
        global $con;

        /* assign the variable with data from the delete button */
        $offerId = $_POST['offerId'];
        $demandId = $_POST['demandId'];

        /* Delete the offer corresponding to the offerid*/
        $delete_offers_query = "DELETE FROM offers WHERE offerId='$offerId'";
        $delete_offers_result = mysqli_query($con, $delete_offers_query)
                             or die(mysqli_error($con));

        header('Location: board.php');
  }
}

//A function which exports an offer to XML
function export_offer ()
{
    global $con;

    /* assign the variable with data from "Export Offer" button*/
    $offerId = $_POST['offerId'];

    /*get the offer and all it's related the data from the database*/
    $offer_export_query = "SELECT o.offerId,
                                  o.text,
                                  o.price,
                                  o.date,
                                  o.pieces,
                                  q.qualityName,
                                  d.demandTitle
                           FROM offers o
                           INNER JOIN demands d
                               on o.demandId = d.demandId
                           INNER JOIN quality q
                               on o.qualityId = q.qualityId
                           WHERE o.offerId = '$offerId';";
    $query_result = mysqli_query($con, $offer_export_query) or
                        die(mysqli_error($con));
    $export_rows = $query_result->fetch_object();

    //create a dom document with encoding utf8
    $domtree = new DOMDocument('1.0', 'UTF-8');

    /* create the root element of the xml tree and append it to
       the dom document*/
    $xmlRoot = $domtree->createElement("offer");
    $offer_export = $domtree->appendChild($xmlRoot);

    //add the offer fields as child elements to the XML
    $offer_export->appendChild(
        $domtree->createElement('demand_title',$export_rows->demandTitle));
    $offer_export->appendChild(
        $domtree->createElement('offer_id',$export_rows->offerId));
    $offer_export->appendChild(
        $domtree->createElement('offer_text',$export_rows->text));
    $offer_export->appendChild(
        $domtree->createElement('offer_price',$export_rows->price));
    $offer_export->appendChild(
        $domtree->createElement('offer_pieces',$export_rows->pieces));
    $offer_export->appendChild(
        $domtree->createElement('offer_quality',$export_rows->qualityName));
    $offer_export->appendChild(
        $domtree->createElement('offer_date',$export_rows->date));

    // format the dom document
     $domtree->formatOutput = true;
    // print out the generate dom document
    echo $domtree->saveXML();
}
?>
