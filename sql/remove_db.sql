-- Removes the DB and its user
--
-- Author: Andreas Zweili
-- 2017-02-03
-- MariaDB 10.0.27

drop database if exists webshopdb;
drop user webshop@localhost;
