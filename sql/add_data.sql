-- Adds three dummy users to the db
-- 
-- Author: Andreas Zweili
-- 2017-02-03
-- MariaDB 10.0.27 

use webshopdb;

insert into users (userLogin, userPass, userEmail)
    values ('demander','password','demander@example.com'),
    ('provider','password','provider@example.com'),
    ('admin','password','admin@example.com');

insert into quality (qualityName)
    values ('new'),
    ('used');
