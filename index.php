<?php
include 'functions.php';
?>
<!DOCTYPE html>
<html>
<head>
  <!-- enable utf-8 encoding for umlauts etc.-->
  <meta charset="utf-8">
  <!-- Description of what this dose -->
  <meta name ="viewport" content="width=device-width, initial-scale=1">
  <!-- link to the default css file -->
  <link rel="stylesheet" href="css/loginstyle.css"/>
  <title>Businessstreamline</title>
</head>

<body>
    <div>
        <header>
            <h1>B2B2-Plattform</h1>
        </header>
    </div>
    <div class="form">
        <h1>Log In</h1>
        <form action="<?= $_SERVER['PHP_SELF'] ?>" method="post" name="login">
        <input type="text" name="username" placeholder="Username" required />
        <input type="password" name="password" placeholder="Password" required />
        <input name="submit" type="submit" value="Login"/>
        </form>
        <p>Not registered yet? <a href='registration.php'>Register Here</a></p>
        <?php
        login();
        ?>
    </div>
</body>
</html>
